_______________________________________________
<div class="pt-5 pl-4 pr-4">
    <h4>Danh sách người dùng</h4>
    <div id = "table">
    </div>
    <div class="modal" id="modalUpdate">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Thông tin người dùng</h4>
                    <button type="button" class="close" data-dismiss="modal" id="cancel_update_user">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <?php
                    require_once $_SERVER['DOCUMENT_ROOT'].'\MVC\Views\Form\form_info_user.php';
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="modal mt-5" id="modalDelete">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Bạn có chắc chắn xóa người dùng này không?</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <button id="delete_user" class="btn btn-success" data-dismiss="modal">Xóa</button>
                    <button id ="cancel" class="btn btn-danger" data-dismiss="modal">Hủy</button>
                </div>
            </div>
        </div>
    </div>
    <script src="../../User Management/MVC/Core/JS/Validate/Validator.js"></script>
    <script>
        //Validate Form Update
        Validator({
            form: '#form_info_user',
            errorSelector: '.form-group .error-mesage',
            rules:[
                Validator.IsRequire('#full_name'),
                Validator.IsRequire('#address'),
                Validator.IsRequire('#phone_number'),
                Validator.IsEmail('#email'),
            ]
        })
    </script>
</div>
----------------------------------------------------
