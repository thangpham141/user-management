<!DOCTYPE html>
<html lang="en">
<head>
    <title>Quản lý người dùng</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="../../User Management/MVC/Core/JS/main.js"></script>
    <script src="../../User Management/MVC/Core/JS/Validate/Validator.js"></script>
</head>
<body>
<div class="d-flex">
    <div class="col-sm-9"><h2>Quản lý người dùng</h2></div>
    <div class="col-sm-3 mt-2">
        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#signUp">
            Đăng ký
        </button>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#signIn">
            Đăng nhập
        </button>
    </div>
    <div class="modal" id="signUp">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Đăng ký</h4>
                    <button type="button" class="close" data-dismiss="modal" id="cancel_sign_up">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <?php
                    require_once $_SERVER['DOCUMENT_ROOT'] . '\MVC\Views\Form\form_sign_up.php';
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" id="signIn">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Đăng nhập</h4>
                    <button type="button" class="close" data-dismiss="modal" id="cancel_sign_in">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <?php
                    require_once $_SERVER['DOCUMENT_ROOT'] . '\MVC\Views\Form\form_sign_in.php';
                    ?>
                </div>
            </div>
        </div>
    </div>

</div>
<script>
    //Validate Sign In
    Validator({
        form: '#form_sign_in',
        errorSelector: '.error-message',
        rules: [
            Validator.IsRequire('#account_name_sign_in'),
            Validator.IsRequire('#password_sign_in'),
        ]
    })
    //Validate Sign Up
    Validator({
        form: '#form_sign_up',
        errorSelector: '.error-message',
        rules: [
            Validator.IsRequire('#full_name_sign_up'),
            Validator.IsRequire('#address_sign_up'),
            Validator.IsEmail('#email_sign_up'),
            Validator.IsRequire('#phone_number_sign_up'),
            Validator.IsRequire('#account_name_sign_up'),
            Validator.IsRequire('#password_sign_up'),
        ]
    })

</script>
