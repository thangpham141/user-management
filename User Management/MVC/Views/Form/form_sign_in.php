<div class="">
        <form id ="form_sign_in">
            <div class="form-group">
                <label for="account_name_sign_in">Tên tài khoản:</label>
                <input type="text" class="form-control" id="account_name_sign_in" placeholder="Nhập tên tài khoản" name="account_name_sign_in">
                <span class="error-message text-danger"></span>
            </div>
            <div class="form-group">
                <label for="password_sign_in">Mật khẩu:</label>
                <input type="password" class="form-control" id="password_sign_in" placeholder="Nhập mật khẩu" name="password_sign_in">
                <span class="error-message text-danger"></span>
            </div>
            <button type="submit" class="btn btn-primary mb-2" name="sign_in" id="sign_in">Đăng nhập</button>
        </form>
</div>

