<form id="form_info_user">
    <div>
        <input type="hidden" id="user_id">
    </div>
    <div class="form-group d-flex pt-2">
        <label class="col-sm-2  pt-2" for="full_name">Họ và tên:</label>
        <input type="text" class="form-control col-sm-6" id="full_name" placeholder="Nhập họ và tên" name="full_name">
        <span class="col-sm-3 pt-2 text-danger error-message"></span>
    </div>
    <div class="form-group d-flex">
        <label class="col-sm-2  pt-2" for="gender">Giới tính:</label><br>
        <label>
            <select name="gender" id="gender" class="form-control">
                <option value="male">Nam</option>
                <option value="female">Nữ</option>
                <option value="other">Khác</option>
            </select>
        </label>
    </div>
    <div class="form-group d-flex">
        <label class="col-sm-2 pt-2" for="date_of_birth">Ngày sinh:</label>
        <input type="date" class="form-control col-sm-6" id="date_of_birth" name="date_of_birth">
    </div>
    <div class="form-group d-flex">
        <label class="col-sm-2 pt-2" for="address">Địa chỉ:</label>
        <input type="text" class="form-control col-sm-6" id="address" placeholder="Nhập địa chỉ" name="address">
        <span class="error-message col-sm-3 pt-2 text-danger"></span>
    </div>
    <div class="form-group d-flex">
        <label class="col-sm-2 pt-2" for="email">Email:</label>
        <input type="email" class="form-control col-sm-6" id="email" placeholder="Nhập email" name="email">
        <span class="error-message col-sm-3 pt-2 text-danger"></span>
    </div>
    <div class="form-group d-flex">
        <label class="col-sm-2 pt-2" for="phone_number">Số điện thoại:</label>
        <input type="text" class="form-control col-sm-6" id="phone_number" placeholder="Nhập số điện thoại" name="phone_number">
        <span class="error-message col-sm-3 pt-2 text-danger"></span>
    </div>

    <button id="update_user" type="submit" class="btn btn-primary mb-2" name="update_user">Lưu</button>

</form>