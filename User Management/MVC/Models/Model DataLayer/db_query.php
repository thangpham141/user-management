<?php
include $_SERVER['DOCUMENT_ROOT'].'\MVC\Models\Model DataLayer\db_connection.php';
//Truy vấn insert, update, delete
function Execute($sql){
    $result =  DbConnection()->query($sql);
    DbConnection()->close();
    if($result){
        return $result;
    }
    else{
        return "Truy vấn không thành công!";
    }
}
//Lấy dữ liệu từ Db (SELECT)
function ExecuteResult($sql){
    $resultSet = DbConnection() ->query($sql);
    if($resultSet){
        $list = [];
        while ($row = mysqli_fetch_array($resultSet,1)){
            $list[] = $row;
        }
        DbConnection()->close();
        return $list;
    }
    else{
        return "Truy vấn không thành công!";
    }
}
