<?php
//Validate số điện thoại
function ValPhoneNumber($phoneNumber){
    if(strlen($phoneNumber)>10){
        return "Vui lòng nhập đúng số điện thoại!";
    }
    elseif (strlen($phoneNumber)==0){
        return "Vui lòng điền đủ thông tin!";
    }
    return "";
}

//Validate Email
function ValEmail($email){
    ValRequired($email);
    if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
        return "Email không đúng định dạng!";
    }
    elseif ($email == null){
        return "Vui lòng điền đủ thông tin!";
    }
    return "";
}

//Validate bắt buộc nhập
function ValRequired($required){
    if (empty($required)||$required==""||$required==null){
        return "Vui lòng điền đủ thông tin!";
    }
    return "";
}