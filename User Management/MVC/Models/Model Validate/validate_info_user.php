<?php
//Validate giới tính
function ValGender($gender){
    if($gender==1){
        return "Nam";
    }
    elseif ($gender==2){
        return "Nữ";
    }
    else{
        return "Khác";
    }
}

//Validate ngày sinh
function ValDOB($dateOfBirth){
    $arrayDOB = explode('-',$dateOfBirth);
    return $arrayDOB[2]."/".$arrayDOB[1]."/".$arrayDOB[0];
}

