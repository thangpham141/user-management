<?php
require_once $_SERVER['DOCUMENT_ROOT'].'\MVC\Models\Model Validate\validate_info_user.php';
require_once $_SERVER['DOCUMENT_ROOT'].'\MVC\Models\Model DataLayer\db_query.php';
// Lấy danh sách người dùng
function GetListUsers()
{
    $value = '<table class="table table-hover">
            <thead>
                <tr>
                    <th>STT</th>
                    <th>Họ và tên</th>
                    <th>Giới tính</th>
                    <th>Ngày sinh</th>
                    <th>Địa chỉ</th>
                    <th>Email</th>
                    <th>Số điện thoại</th>
                    <th></th>
                </tr>
            </thead>';
    $value .= "<tbody>";
    $sql = 'SELECT * FROM user';
    $listUser = ExecuteResult($sql);
    $index = 1;
    foreach ($listUser as $user) {
        $value .= '           <tr>
                                <td>' . $index . '</td>
                                <td>' . $user['FullName'] . '</td>
                                <td>' . ValGender($user['Gender']) . '</td>
                                <td>' . ValDOB($user['DateOfBirth']) . '</td>
                                <td>' . $user['Address'] . '</td>
                                <td>' . $user['Email'] . '</td>
                                <td>' . $user['PhoneNumber'] . '</td>
                                <td>
                                    <button data-toggle="modal" data-target="#modalUpdate" type=\'button\' class=\'btn btn-warning\' id=\'btn_edit\' data-id ='.$user['UserID'].' >
                                    Sửa
                                    </button>
                                    <button data-toggle="modal" data-target="#modalDelete" type=\'button\' class=\'btn btn-danger\' id=\'btn_delete\' data-id1 ='.$user['UserID'].' >
                                    Xóa
                                    </button>
                                </td>
                            </tr>';
        $index++;
    }
    $value .= '</tbody>
                 </table>';
    echo json_encode(['status' => 'success', 'html' => $value]);
}

//Lấy dữ liệu của người dùng bằng ID
function GetUser()
{
    $UserID = $_GET['id'];
    $sql = 'SELECT UserID,FullName,Gender,DateOfBirth,Address,Email,PhoneNumber FROM user WHERE UserID = '.$UserID;
    $user = ExecuteResult($sql);
    $userData = [];
    $userData[0] = $user[0]['UserID'];
    $userData[1] = $user[0]['FullName'];
    $userData[2] = $user[0]['Gender'];
    $userData[3] = $user[0]['DateOfBirth'];
    $userData[4] = $user[0]['Address'];
    $userData[5] = $user[0]['Email'];
    $userData[6] = $user[0]['PhoneNumber'];

    echo json_encode($userData);
}

//Cập nhật thông tin người dùng
function UpdateUser(){
    $UserID = $_POST['UserID'];
    $FullName = $_POST['FullName'];
    $Gender = $_POST['Gender'];
    if($_POST['Gender']=="male"){
        $Gender = 1;
    }
    elseif ($_POST['Gender']=="female"){
        $Gender = 2;
    }
    else{
        $Gender = 3;
    }
    $DateOfBirth = $_POST['DateOfBirth'];
    $Address = $_POST['Address'];
    $Email = $_POST['Email'];
    $PhoneNumber = $_POST['PhoneNumber'];
    $sql = "UPDATE user SET FullName = '$FullName', Gender = '$Gender',DateOfBirth = '$DateOfBirth',
            Address = '$Address' ,Email = '$Email',PhoneNumber = '$PhoneNumber'
            WHERE UserID = $UserID";
    $result = Execute($sql);
    if($result){
        echo "Cập nhật người dùng thành công!";
    }
    else{
        echo "Cập nhật không thành công!";
    }
}
//Xóa người dùng
function DeleteUser(){
    if(isset($_GET['id'])){
        $UserID = $_GET['id'];
        $sql = "DELETE FROM user WHERE UserID = $UserID";
        $result = Execute($sql);
        if($result){
            echo "Xóa người dùng thành công!";
        }
        else{
            echo "Xóa người dùng không thành công!";
        }
    }
    else{
        echo "Có lỗi xảy ra! Vui lòng liên hệ 09009000!";
    }
}
