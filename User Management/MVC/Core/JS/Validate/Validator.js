function Validator(options) {
    //Hàm thực hiện validate
    function validate(inputElement,rule){
        var value = inputElement.val();
        //console.log(inputElement);
        var parent = inputElement.parent();
        //console.log(parent);
        //var child = parent.children(options.errorSelector);
        var child = parent.children(".error-message");
        //console.log(child);
        var errorMessage= rule.test(value);
        if(errorMessage){
            child.html(errorMessage);
            inputElement.addClass("border-danger");
            //inputElement.attr('placeholder',errorMessage)
        }
        else {
            child.html("");
            inputElement.removeClass("border-danger");
        }
    }
    //Lấy Element của form
    var formElement = $(options.form);
    if(formElement){
        options.rules.forEach(function (rule){
            var inputElement = $(rule.selector);
            if(inputElement) {
                inputElement.blur(function () {
                    validate(inputElement,rule);
                });
                //Xử lí khi người dùng nhập
                inputElement.on('input', function (){
                    inputElement.removeClass("border-danger");
                    var parent = inputElement.parent();
                    var child = parent.children(".error-message");
                    child.html("");
                })
            }
        })
    }
}
    Validator.IsRequire = function (selector){
        return {
            selector: selector,
            test: function (value){
                return value.trim() ? undefined : "Vui lòng nhập trường này!";
            }
        }
    }
    Validator.IsEmail = function (selector){
        return {
            selector: selector,
            test: function (value){
                var regex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
                if(regex.test(value)){
                    return undefined;
                }
                else {
                    if(value==""){
                        return "Vui lòng nhập email vào trường này!";
                    }
                    else {
                        return "Email chưa đúng định dạng!";
                    }
                }
            }
        }
    }