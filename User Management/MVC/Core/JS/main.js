$(document).ready(function ()
{
    ViewListUsers();
    GetInfoUser();
    UpdateUser();
    DeleteUser();
    SignUp();
    SignIn();
})
//Hiện danh sách người dùng
function ViewListUsers(){
    $.ajax(
        {
            url: '../../User Management/MVC/Models/Model Common/model_get_list_users.php',
            type: 'get',
            success: function (data){
                data = JSON.parse(data);
                if(data.status=='success'){
                    $("#table").html(data.html);
                }

            }
        }
    )
}

//Lấy dữ liệu người dùng
function GetInfoUser(){
    $(document).on('click','#btn_edit',function (){
        var ID = $(this).attr('data-id');
        $.ajax(
            {
                url: '../../User Management/MVC/Models/Model Common/model_get_user.php',
                type: 'get',
                data: {'id':ID},
                dataType: 'JSON',
                success: function (data){
                    $("#user_id").val(data[0])
                    $("#full_name").val(data[1]);
                    if(data[2]==1){
                        $("#gender").val("male");
                    }
                    else if(data[2]==2){
                        $("#gender").val("female");
                    }
                    else{
                        $("#gender").val("other");
                    }
                    $("#date_of_birth").val(data[3]);
                    $("#address").val(data[4]);
                    $("#email").val(data[5]);
                    $("#phone_number").val(data[6]);
                }
            }
        )
    })
    // $('#btn_edit').click(function (){
    //     var ID = $(this).attr('data-id')
    //    console.log(ID);
    // });
}

//Update thông tin người dùng
function UpdateUser(){
    $(document).on('click','#cancel_update_user',function (){
        //$("#cancel_update_user").trigger('reset');
        $("#form_info_user span").html("");
        $("#form_info_user input").removeClass("border-danger");
    })

    $(document).on('click','#update_user',function (){
        var UserID = $("#user_id").val();
        var FullName = $("#full_name").val();
        var Gender = $("#gender").val();
        var DateOfBirth = $("#date_of_birth").val();
        var Address = $("#address").val();
        var Email = $("#email").val();
        var PhoneNumber = $("#phone_number").val();
        console.log(UserID);
        $.ajax(
            {
                url: '../../User Management/MVC/Models/Model Common/model_update_user.php',
                type: 'post',
                data: { 'UserID':UserID,
                        'FullName':FullName,
                        'Gender':Gender,
                        'DateOfBirth':DateOfBirth,
                        'Address':Address,
                        'Email':Email,
                        'PhoneNumber':PhoneNumber,
                },
                success: function (data){
                    alert(data);
                    ViewListUsers();
                }
            }
        )
    })
}
//Xóa người dùng
function DeleteUser(){
    $(document).on('click','#btn_delete',function (){
        var ID = $(this).attr('data-id1');
        $(document).on('click','#delete_user',function (){
            $.ajax({
                url: '../../User Management/MVC/Models/Model Common/model_delete.php',
                type: 'get',
                data: {'id':ID},
                success: function (data){
                    alert(data);
                    ViewListUsers();
                }
            })
        })
    });
}
//Đăng ký người dùng
function SignUp(){
    $(document).on('click','#cancel_sign_up',function (){
        $("#form_sign_up").trigger('reset');
        $("#form_sign_up span").html("");
        $("#form_sign_up input").removeClass("border-danger");
    })
    $(document).on('click','#sign_up',function (){
        var FullName = $('#full_name_sign_up').val();
        var Gender = $('#gender_sign_up').val();
        var DateOfBirth = $('#date_of_birth_sign_up').val();
        var Address = $('#address_sign_up').val();
        var Email = $('#email_sign_up').val();
        var PhoneNumber = $('#phone_number_sign_up').val();
        var AccountName = $('#account_name_sign_up').val();
        var PassWord = $('#password_sign_up').val();
        $.ajax({
            url:'../../User Management/MVC/Models/Model Form/model_sign_up.php',
            type: 'post',
            data: {'FullName':FullName,
                'Gender': Gender,
                'DateOfBirth':DateOfBirth,
                'Address':Address,
                'Email':Email,
                'PhoneNumber':PhoneNumber,
                'AccountName':AccountName,
                'PassWord':PassWord,
            },
            success: function (data){
                alert(data);
                //$("form").trigger('reset');
            }
        })
    })
}

//Đăng nhập
function SignIn(){
    $(document).on('click','#cancel_sign_in',function (){
        $("#form_sign_in").trigger('reset');
        $("#form_sign_in span").html("");
        $("#form_sign_in input").removeClass("border-danger");
    })
    $(document).on('click','#sign_in',function (){
       var AccountName = $('#account_name_sign_in').val();
       var Password = $('#password_sign_in').val();
       $.ajax({
           url:'../../User Management/MVC/Models/Model Form/model_sign_in.php',
           type: 'post',
           data: {
               'AccountName': AccountName,
               'Password': Password,
           },
           success: function (data){
               if(data=="Đăng nhập thành công!"){
                   $('#signIn').modal('hide');
                   alert(data);
                   $("#form_sign_in").trigger('reset');
               }
                else {
                   alert(data);
               }
           }
       })
    });
}