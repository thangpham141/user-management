<?php
class App{
    protected $controller="Home";
    protected $action="Say Hi";
    protected $param;

    function __construct()
    {
        require_once $_SERVER["DOCUMENT_ROOT"] . "\MVC\Views\Layout\Header.php";
        require_once $_SERVER["DOCUMENT_ROOT"] . "\MVC\Views\Layout\Content.php";
        require_once $_SERVER["DOCUMENT_ROOT"] . "\MVC\Views\Layout\Footer.php";
    }
}
